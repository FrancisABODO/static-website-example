FROM nginx
COPY static /usr/share/nginx/html

# Expose is NOT supported by Heroku
# EXPOSE 80

# Run the app.  CMD is required to run on Heroku
#CMD ["usr/sbin/nginx", "-g", "daemon off;"]